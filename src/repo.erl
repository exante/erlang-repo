-module(repo).

-export([
         %% Query api
         query/1, query/2,

         all/1, all/2, all/3,
         zlist/2, zlist/3, zlist/4,
         get_one/1, get_one/2, get_one/3,
         insert/2, insert/3, insert/4,
         upsert/2, upsert/3, upsert/4,
         update/2, update/3, update/4,
         set/2, set/3,
         delete/1, delete/2, delete/3, delete/4
        ]).

-include_lib("equery/include/equery.hrl").
-include_lib("epgsql/include/epgsql.hrl").

-type schema() :: #{
        fields => #{ atom() => #{_ => _} },
        table => binary(),
        _ => _
    }.

-export_type([schema/0]).

query(Model) -> query(Model, []).
query(Model, QList) -> pipe(Model, QList).

%% === all/1,2,3 ===============================================================

all(Info) ->
    all_(fun pgpool:with/1, query(Info)).
all(C, Info) when is_pid(C) ->
    all_(wrap_connection(C), query(Info));
all(Info, QList) ->
    all_(fun pgpool:with/1, query(Info, QList)).
all(C, Info, QList) when is_pid(C) ->
    all_(wrap_connection(C), query(Info, QList)).

all_(FunC, Query) ->
    {Sql, Args, Fields} = to_sql(qsql:select(Query)),
    {ok, _Columns, Rows} = FunC(fun(C) -> epgsql:equery(C, Sql, Args) end),
    Constructor = get_constructor(Fields),
    [Constructor(R) || R <- Rows].

%% === zlist/2,3,4 =============================================================

zlist(Info, FunZ) ->
    zlist_(fun pgpool:transaction/1, query(Info), FunZ).
zlist(C, Info, FunZ) when is_pid(C) ->
    zlist_(wrap_connection(C), query(Info), FunZ);
zlist(Info, QList, FunZ) ->
    zlist_(fun pgpool:transaction/1, query(Info, QList), FunZ).
zlist(C, Info, QList, FunZ) when is_pid(C) ->
    zlist_(wrap_connection(C), query(Info, QList), FunZ).

zlist_(FunC, Query, FunZ) ->
    {Sql, Args, Fields} = to_sql(qsql:select(Query)),
    Constructor = get_constructor(Fields),
    Portal = io_lib:print(make_ref()),
    {ok, NRows} = application:get_env(repo, fetch_by),
    FunC(fun(C) ->
        case epgsql:parse(C, Sql) of
            {ok, Statement} ->
                ok = epgsql:bind(C, Statement, Portal, Args),
                try FunZ(zlist(C, Statement, Portal, NRows, Constructor))
                after ok = epgsql:sync(C)
                end;
            {error, R} -> throw({pgsql_exec_error, R})
        end
    end).

zlist(C, Statement, Portal, NRows, Constructor) ->
    case epgsql:execute(C, Statement, Portal, NRows) of
        {error, R} ->
            throw({pgsql_exec_error, R});
        {partial, Rows} ->
            ZList = zlist:map(Constructor, zlist:from_list(Rows)),
            zlist:append(ZList, fun() -> (zlist(C, Statement, Portal, NRows, Constructor))() end);
        {ok, []} -> zlist:empty();
        {ok, Rows} -> zlist:map(Constructor, zlist:from_list(Rows))
    end.

%% === get_one/1,2,3 ===========================================================

get_one(Info) ->
    get_one_(fun pgpool:with/1, query(Info)).
get_one(C, Info) when is_pid(C) ->
    get_one_(wrap_connection(C), query(Info));
get_one(Info, QList) ->
    get_one_(fun pgpool:with/1, query(Info, QList)).
get_one(C, Info, QList) when is_pid(C) ->
    get_one_(wrap_connection(C), query(Info, QList)).

get_one_(FunC, Query) ->
    case all_(FunC, Query) of
        [] -> {error, not_found};
        [M] -> {ok, M};
        Multiple -> throw({multiple_result, Multiple})
    end.

%% === insert/2,3,4 =========================================================

insert(Info, M) ->
    insert_(fun pgpool:transaction/1, Info, M, undefined, created).
insert(C, Info, M) when is_pid(C) ->
    insert_(wrap_connection(C), Info, M, undefined, created);
insert(Info, M, HookOpts) ->
    insert_(fun pgpool:transaction/1, Info, M, HookOpts, created).
insert(C, Info, M, HookOpts) ->
    insert_(wrap_connection(C), Info, M, HookOpts, created).

insert_(FunC, Info, M, HookOpts, Event) ->
    Query = query(Info),
    Model = get_model(Query),
    Schema = q:get(schema, Query),
    WithDiff = maps:get(withDiff, Schema, false) andalso Event =:= updated, %% Only for upsert
    case erlang:function_exported(Model, do_insert, 3) of
        true -> FunC(fun(C) -> Model:do_insert(C, M, HookOpts) end);
        false ->
            store(FunC, fun(Q) ->
                add_diff(WithDiff, qsql:insert(Q), Schema)
            end, Query, M, Event, HookOpts, WithDiff)
    end.

%% === upsert/2,3,4 =========================================================

upsert(Info, M) ->
    upsert_(fun pgpool:transaction/1, Info, M, undefined).
upsert(C, Info, M) when is_pid(C) ->
    upsert_(wrap_connection(C), Info, M, undefined);
upsert(Info, M, HookOpts) ->
    upsert_(fun pgpool:transaction/1, Info, M, HookOpts).
upsert(C, Info, M, HookOpts) ->
    upsert_(wrap_connection(C), Info, M, HookOpts).

upsert_(FunC, Info, M, HookOpts) ->
    Query = query(Info),
    #{fields := Fields} = q:get(schema, Query),
    IndexFields = maps:fold(
        fun (K, #{index := true}, Acc) -> [K|Acc];
            (_, _, Acc) -> Acc
        end, [], Fields),
    FieldsToUpdate = maps:fold(
        fun (_, #{readOnly := true}, Acc) -> Acc;
            (K, _, Acc) -> [K|Acc]
        end, [], Fields),

    QueryR = q:on_conflict(IndexFields, fun(Data) ->
        maps:with(FieldsToUpdate, lists:last(Data))
    end, Query),
    insert_(FunC, QueryR, M, HookOpts, updated).

%% === update/2,3 ===========================================================

update(Info, M) ->
    update_(fun pgpool:transaction/1, Info, M, undefined).
update(C, Info, M) when is_pid(C) ->
    update_(wrap_connection(C), Info, M, undefined);
update(Info, M, HookOpts) ->
    update_(fun pgpool:transaction/1, Info, M, HookOpts).
update(C, Info, M, HookOpts) ->
    update_(wrap_connection(C), Info, M, HookOpts).

update_(FunC, Info, M, HookOpts) ->
    Query = query(Info),
    #{fields := Fields} = Schema = q:get(schema, Query),
    WithDiff = maps:get(withDiff, Schema, false),
    store(FunC, fun(Q) ->
        QR = pipe(Q, [
            q:where(fun([Data|_]) ->
                maps:fold(
                    fun (IndexF, #{index := true}=Opts, S) ->
                            S andalso maps:get(IndexF, Data) =:= getter(IndexF, Opts);
                        (_, _, S) -> S
                    end, true, Fields)
            end)
        ]),
        add_diff(WithDiff, qsql:update(QR), Schema)
    end, Query, M, updated, HookOpts, WithDiff).

%% === set/2,3 ===========================================================

set(Info, QList) ->
    set_(fun pgpool:transaction/1, Info, QList).
set(C, Info, QList) when is_pid(C) ->
    set_(wrap_connection(C), Info, QList).

set_(FunC, Info, QList) ->
    Query = query(Info, QList),
    #{fields := Fields} = q:get(schema, Query),
    QR = q:set(fun(S, _) ->
        maps:map(fun(K, V) ->
            Type = maps:get(type, maps:get(K, Fields), undefined),
            (encoder(Type))(V)
        end, S)
    end, Query),
    {Sql, Args, RFields} = to_sql(qsql:update(QR)),
    Constructor = get_constructor(RFields),
    FunC(fun(C) ->
        case epgsql:equery(C, Sql, Args) of
            {ok, 0} -> [];
            {ok, _, _Columns, Rows} -> lists:map(Constructor, Rows)
        end
    end).

%% === delete/1,2,3,4 =========================================================

delete(Info) ->
    delete_(fun pgpool:transaction/1, query(Info), undefined).
delete(C, Info) when is_pid(C) ->
    delete_(wrap_connection(C), query(Info), undefined);
delete(Info, QList) ->
    delete_(fun pgpool:transaction/1, query(Info, QList), undefined).
delete(C, Info, QList) when is_pid(C) ->
    delete_(wrap_connection(C), query(Info, QList), undefined);
delete(Info, QList, HookOpts) ->
    delete_(fun pgpool:transaction/1, query(Info, QList), HookOpts).
delete(C, Info, QList, HookOpts) ->
    delete_(wrap_connection(C), query(Info, QList), HookOpts).

delete_(FunC, Query, HookOpts) ->
    Model = get_model(Query),
    BeforeHook = get_hook(Model, before_delete, 3),
    AfterHook = get_hook(Model, after_delete, 3),
    ChangedHook = get_hook(Model, changed, 5),
    FunC(fun(C) when is_pid(C) ->
        case BeforeHook(C, Query, HookOpts) of
            {ok, Q1} ->
                {Sql, Args, Fields} = to_sql(qsql:delete(Q1)),
                Constructor = get_constructor(Fields),
                DiffFun = diff_fun(Fields, false),
                Result = epgsql:equery(C, Sql, Args),
                case Result of
                    {ok, _} -> {ok, []};
                    {ok, _, _, Rows} ->
                        RowsR = lists:map(fun(R) ->
                            {M, FieldsChanged} = DiffFun(Constructor(R)),
                            ResultM = AfterHook(C, M, HookOpts),
                            ChangedHook(C, deleted, ResultM, FieldsChanged, HookOpts),
                            ResultM
                        end, Rows),
                        {ok, RowsR};
                    {error, _} = V -> V

                end;
            {error, _} = V -> V
        end
    end).

%% =============================================================================
%% Utils
%% =============================================================================

get_model(Q) ->
    maps:get(model, q:get(schema, Q), undefined).

pipe(Model, QList) when is_atom(Model); is_map(Model) ->
    pipe(q:from(Model), QList);
pipe(Query, QList) ->
    q:pipe(Query, where(QList)).

where(QList) when is_list(QList) -> QList;
where(Map) when is_map(Map) ->
    [repo_utils:like(Map)].

maybe_list([], _Fun) ->
    {ok, []};
maybe_list(M, Fun) when is_list(M) ->
    Fun(M);
maybe_list(M, Fun) ->
    case Fun([M]) of
        {ok, [R]} -> {ok, R};
        {error, [{_N, R}]} -> {error, R}
    end.

store(FunC, SqlF, Query, DataMaybeList, Event, HookOpts, WithDiff) ->
    maybe_list(DataMaybeList, fun(DataList) ->
        store_(FunC, SqlF, Query, DataList, Event, HookOpts, WithDiff)
    end).

store_(FunC, SqlF, Query0, DataList, Event, HookOpts, WithDiff) ->
    Query = pipe(Query0, [
        fun(Q) ->
            #{fields := Fields} = q:get(schema, Q),
            q:set(fun(_) ->
                maps:fold(
                    fun (_, #{readOnly := true}, S) -> S;
                        (F, Opts, S) -> maps:put(F, getter(F, Opts), S)
                    end, #{}, Fields)
            end, Q)
        end
    ]),
    {Sql, ArgsFields, ReturnFieldsData} = to_sql(SqlF(Query)),
    Model = get_model(Query),
    DiffFun = diff_fun(ReturnFieldsData, WithDiff),
    BeforeHook = get_hook(Model, before_save, 3),
    AfterHook = get_hook(Model, after_save, 4),
    ChangedHook = get_hook(Model, changed, 5),
    ToDb = get_hook(Model, to_db, 1),
    Constructor = get_constructor(ReturnFieldsData),

    ApplyHooks = fun(C, R, M) ->
        {Result, FieldsChanged} = DiffFun(Constructor(R)),
        UpdatedResult = AfterHook(C, M, Result, HookOpts),
        case FieldsChanged of
            [] -> ok;
            _ -> ChangedHook(C, Event, UpdatedResult, FieldsChanged, HookOpts)
        end,
        UpdatedResult
    end,
    FunC(fun(C) ->
        case enumerate_error_writer_map(fun(M) -> BeforeHook(C, M, HookOpts) end, DataList) of
            {ok, PreprocessedData} ->
                PreparedData = [data(ArgsFields, ToDb(M)) || M <- PreprocessedData],
                {ok, S} = epgsql:parse(C, Sql),
                QueryData = [{S, M} || M <- PreparedData],
                QueryResult = epgsql:execute_batch(C, QueryData),

                enumerate_error_writer_map(
                    fun ({{ok, 0}, _}) -> {error, not_found};
                        ({{ok, [R]}, M}) -> {ok, ApplyHooks(C, R, M)};
                        ({{ok, _, [R]}, M}) -> {ok, ApplyHooks(C, R, M)};
                        ({{error, #error{code = <<"23505">>,codename=unique_violation}}, _}) ->
                            {error, duplicate};
                        ({{error, _Reason}=Err, _}) -> Err
                    end, lists:zip(QueryResult, PreprocessedData));
            V -> V
        end
    end).

getter(F, Opts) ->
    Encoder = encoder(maps:get(type, Opts, undefined)),
    fun(M) ->
        case maps:find(F, M) of
            {ok, V} -> Encoder(V);
            error -> null
        end
    end.

diff_fun({model, _, RFields}, true) ->
    FieldsList = [K || {K, _} <- RFields, K =/= '_diff'],
    fun(#{'_diff' := Diff}=R) ->
        ChangedFields = [K || {K, true} <- lists:zip(FieldsList, Diff)],
        {maps:remove('_diff', R), ChangedFields}
    end;
diff_fun({model, _, RFields}, false) ->
    FieldsList = [K || {K, _} <- RFields],
    fun(R) ->
        {R, FieldsList}
    end;
diff_fun(_T, _WithDiff) ->
    fun(R) ->
        {R, []}
    end.

add_diff(true, SqlAst, #{fields := Fields} = Schema) ->
    IndexFields = [F || {F, #{index := true}} <- maps:to_list(Fields)],
    qsql:select(pipe(query(Schema), [
        q:with(SqlAst, fun(UpdatedT) ->
            q:join(right, UpdatedT, fun([A, B]) ->
                lists:foldl(fun(K, Acc) ->
                    maps:get(K, A) =:= maps:get(K, B) andalso Acc
                end, true, IndexFields)
            end)
        end),
        q:select(fun([T1|Rest]) ->
            T2 = lists:last(Rest),
            T2#{'_diff' => array_of([
                is_distinct_from(maps:get(K, T1), maps:get(K, T2))
                || K <- maps:keys(T2)
            ], boolean)}
        end)
    ]));
add_diff(false, SqlAst, _Schema) -> SqlAst.

array_of(List, Type) ->
    qast:exp([
        qast:raw("ARRAY["),
        qast:join(List, qast:raw(",")),
        qast:raw("]")
    ], #{type => {array, Type}}).

is_distinct_from(A, B) ->
    qast:exp([
        A, qast:raw(" IS DISTINCT FROM "), B
    ], #{type => boolean}).

get_constructor({model, Model, FieldsData}) when is_list(FieldsData) ->
    {Fields, FieldsOpts} = lists:unzip(FieldsData),
    Decoders = lists:map(fun(Opts) ->
        decoder(maps:get(type, Opts, undefined))
    end, FieldsOpts),
    FromDb = (get_hook(Model, from_db, 1))(Fields),
    fun(TupleData) ->
        FromDb(
            lists:zipwith(
                fun(C, D) -> C(D) end,
                Decoders,
                tuple_to_list(TupleData)))
    end;
get_constructor(FieldType) ->
    Decoder = decoder(FieldType),
    fun({V}) -> Decoder(V) end.

data(FieldsData, M) ->
    lists:map(
        fun (Getter) when is_function(Getter, 1) -> Getter(M);
            (Value) -> Value
        end, FieldsData).

get_hook(Model, Name, Arity) when is_atom(Model) ->
    Module = case erlang:function_exported(Model, Name, Arity) of
        true -> Model;
        false -> repo_model
    end,
    fun Module:Name/Arity;
get_hook(_Model, Name, Arity) ->
    fun repo_model:Name/Arity.

encoder(Json) when Json =:= jsonb; Json =:= json -> fun jiffy:encode/1;
encoder(_) -> fun id/1.

decoder(Type) ->
    Decoder = decoder_(Type),
    fun (null) -> null; (V) -> Decoder(V) end.
decoder_(Json) when Json =:= jsonb; Json =:= json -> fun (V) -> jiffy:decode(V, [return_maps]) end;
decoder_({record, FieldsData}) -> get_constructor(FieldsData);
decoder_({array, SubType}) ->
    TypeDecoder = decoder(SubType),
    fun(D) -> lists:map(TypeDecoder, D) end;
decoder_(uuid) -> fun normalize_uuid/1;
decoder_(_) -> fun id/1.

id(A) -> A.

normalize_uuid(UUID) ->
    binary:replace(UUID,<<"-">>,<<>>,[global]).

to_sql(QAst) ->
    {Sql, Args} = qast:to_sql(QAst),
    #{type := Fields} = qast:opts(QAst),
    {iolist_to_binary(Sql), Args, Fields}.

enumerate_error_writer_map(Fun, List) ->
    {_, Result, Errors} = lists:foldl(fun(E, {N, Ok, Err}) ->
        N2 = N+1,
        case Fun(E) of
            {ok, R} -> {N2, [R|Ok], Err};
            {error, R} -> {N2, Ok, [{N2,R}|Err]}
        end
    end, {0, [], []}, List),
    case Errors of
        [] -> {ok, lists:reverse(Result)};
        _ -> {error, lists:reverse(Errors)}
    end.

-spec wrap_connection(C) -> fun((fun((C) -> R)) -> R) when
        C :: pgpool:connection().
wrap_connection(C) -> fun(F) -> F(C) end.
